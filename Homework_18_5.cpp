#include <iostream>
#include <string>
#include "Template.h"

int main()
{
    Stack <std::string> itself(6); // ������ ����� ������ � �������� int, ��� ������� ���� ����� 6

    std::cout << "Put elements:\n" << std::endl;
    
    int count = 0;
    while (count++ != 6) // �������� ����� ��������, ���� �������� ������
    {
        std::string def;
        std::cin >> def;
        itself.push(def);
    }
    itself.StackPrint();
    std::cout << "\nRemoving 3 elements from the stack:\n" << std::endl;
    itself.pop();
    itself.pop();
    itself.pop();
    itself.StackPrint();

    return 0;
}




