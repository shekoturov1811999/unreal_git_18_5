#include <iostream>


template <typename temp> // ������� ������ ������
class Stack
{
private:
	int size, height; // ���������� ��� ������� � �������� ��������
	temp* stackVal; // ���� ����� �� ��������

public:
	Stack(int = 8); 
	~Stack();
	bool pop(); 
	bool push(const temp);
	void StackPrint();
};

template <typename temp> //�����������
Stack<temp>::Stack(int par)
{
	size = par;
	stackVal = new temp[size];
	height = -1;
	std::cout << "Constructor\n\n"; 
									
}

template <typename temp> //����������
Stack<temp>::~Stack()
{
	delete[] stackVal;
	std::cout << "\nDestructor\n\n";
}

template <typename temp> // push �������
bool Stack<temp>::push(const temp cur)
{
	if (height == size - 1)
		return false;

	height++;
	stackVal[height] = cur;
	return true;
}

template <typename temp> // pop �������
bool Stack<temp>::pop() 
{
	if (height == -1)
		return false;

	stackVal[height] = " ";
	height--;

	return true;
}

template <typename temp> // ������� ������ �������� �����
void Stack<temp>::StackPrint()
{
	for (int i = size - 1; i >= 0; i--)
		std::cout << "|" << stackVal[i] << std::endl;
}